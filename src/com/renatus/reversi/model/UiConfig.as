package com.renatus.reversi.model {
	
	import starling.display.Sprite;
	import starling.utils.AssetManager;
	/**
	 * 
	 */
	public class UiConfig {
		
		public var cfg:XML; 
		public var locale:XML;
		public var assets:AssetManager;
		public var root:Sprite;
		
	}
}